#!/bin/bash
##TMPVRR
#smtpServer
#smtpPort
#senderMail
#receiverMail
#sleepBetween
#authMode
#authProtocol
#authUser
#authPass
stats=notConf
##VRREND
n="-i"

if [ "$stats" = "notConf" ] && [ "&#" = "" ]; then
        echo -n "Enter SMTP Server Name/IP:- ";
        read smtpServer
	echo -n "Enter SMTP Server Port(25,465,587):- ";
        read smtpPort
        echo -n "Enter Sender Mail Address:- ";
        read senderMail
        echo -n "Enter Receiver Mail Address:- ";
        read receiverMail
        echo "Enter pause inbetween command(in sec):- ";
        read -i "1" -e sleepBetween
	echo "Auth protocol(TLS=t or SSL=s n=sendGrid):- ";
	read -i "TLS" -e authProtocol
        echo -e "Is auth login enabled?(Type \"y\" or \"n\"):- ";
        read -i "NO" -e authMode
	sed $n -e "0,/#authMode/ s/#authMode/authMode=$authMode/" $0
if [ "$authMode" = "y" ] || [ "$authMode" = "yes" ] || [ "$authMode" = "Y" ] || [ "$authMode" = "YES" ]; then
        echo -n "Enter Auth userName:- ";
        read authUser
        echo -n "Enter Auth Pass:- ";
        read -s authPass
        sed $n -e "0,/#authUser/ s/#authUser/authUser=$authUser/" $0
        sed $n -e "0,/#authPass/ s/#authPass/authPass=$authPass/" $0
else
	sed $n -e "0,/#authUser/ s/#authUser/authUser=NA/" $0
	sed $n -e "0,/#authPass/ s/#authPass/authPass=NA/" $0
fi
	sed $n -e "0,/#authProtocol/ s/#authProtocol/authProtocol=$authProtocol/" $0
        sed $n -e "0,/#smtpServer/ s/#smtpServer/smtpServer=$smtpServer/" $0
        sed $n -e "0,/#smtpPort/ s/#smtpPort/smtpPort=$smtpPort/" $0
        sed $n -e "0,/#senderMail/ s/#senderMail/senderMail=$senderMail/" $0
        sed $n -e "0,/#receiverMail/ s/#receiverMail/receiverMail=$receiverMail/" $0
        sed $n -e "0,/#sleepBetween/ s/#sleepBetween/sleepBetween=$sleepBetween/" $0
	sed $n -e "0,/stats=notConf/ s/stats=notConf/stats=conf/" $0
       echo "Data Inserted"
       exit;
fi
#


if [ "$1" = "reload" ] && [ "$stats" = "conf" ]; then
        echo "Enter SMTP Server Name/IP:- ";
        read -i "$smtpServer" -e smtpServer
	echo "Enter SMTP Server Port(25,465,587):- ";
        read -i "$smtpPort" -e smtpPort
        echo "Enter Sender Mail Address:- ";
        read -i "$senderMail" -e senderMail
        echo "Enter Receiver Mail Address:- ";
        read -i "$receiverMail" -e receiverMail
        echo "Enter pause inbetween command(in sec):- ";
        read -i "1" -e sleepBetween
	echo "Auth protocol(TLS=t or SSL=s n=sendGrid):- ";
	read -i "TLS" -e authProtocol
        echo -e "Is auth login enabled?(Type \"y\" or \"n\"):- ";
        read -i "NO" -e authMode
	sed $n -e "0,/authMode/ s/authMode/authMode=$authMode/" $0
if [ "$authMode" = "y" ] || [ "$authMode" = "yes" ] || [ "$authMode" = "Y" ] || [ "$authMode" = "YES" ]; then
        echo "Enter Auth userName:- ";
        read -i "$senderMail" -e authUser
        echo -n "Enter Auth Pass:- ";
        read -s authPass
        sed $n -e "0,/authUser/ s/authUser/authUser=$authUser/" $0
        sed $n -e "0,/authPass/ s/authPass/authPass=$authPass/" $0
else
	sed $n -e "0,/authUser/ s/authUser/authUser=NA/" $0
	sed $n -e "0,/authPass/ s/authPass/authPass=NA/" $0
fi
	sed $n -e "0,/authProtocol/ s/authProtocol/authProtocol=$authProtocol/" $0
        sed $n -e "0,/smtpServer/ s/smtpServer/smtpServer=$smtpServer/" $0
        sed $n -e "0,/smtpPort/ s/smtpPort/smtpPort=$smtpPort/" $0
        sed $n -e "0,/senderMail/ s/senderMail/senderMail=$senderMail/" $0
        sed $n -e "0,/receiverMail/ s/receiverMail/receiverMail=$receiverMail/" $0
        sed $n -e "0,/sleepBetween/ s/sleepBetween/sleepBetween=$sleepBetween/" $0
       echo "Data Updated"
       exit;
fi
################## DATA #####################################################

encryptUser=`echo -en $authUser | openssl base64`
encryptPass=`echo -en $authPass | base64`
host=`ip route get 8.8.8.8 | grep src | sed 's/.*src \(.*\)$/\1/g'`
count=3
hos=`hostname`

################# Email Edit ###########################

mailSubject="subject:This is the test mail SUBJECT"
#mailBody=
#echo -e "mail generated from: $host\nThis is automated test mail script.\nPlease reply if you recive this mail here:-sagar.khatiwada@f1soft.com"
######################################################
###################### TEST ###########################
#echo $mailSubject
#echo $mailBody
################ VERIFY #########################
########################################
mailStat(){
echo -e "Sending mail from SMTP_Server=\t$smtpServer\nYour hostname=\t\t\t$hos\nIP=\t\t\t\t$host\nMail from:\t\t\t$senderMail --> $receiverMail"
}
mailTls(){
if [ "$authProtocol" = "t" ] || [ "$authProtocol" = "T" ] || [ "$authProtocol" = "TLS" ] || [ "$authProtocol" = "tls" ];
then
        (echo "open $smtpServer $smtpPort";
        sleep 1;
        echo "EHLO $smtpServer";
if [ "$authMode" = "y" ] || [ "$authMode" = "Y" ] || [ "$authMode" = "YES" ] || [ "$authMode" = "yes" ]; then
        echo "AUTH LOGIN";
	sleep 1;
        echo "$encryptUser";
	sleep 1;
	echo "encryptPass";
fi
	sleep 1;
        echo -e "MAIL from:$senderMail";
        sleep 1;
        echo -e "RCPT to:$receiverMail";
        sleep 1;
        echo "DATA";
        echo "From:$senderMail";
        echo "To:$receiverMail";
        sleep 1;
        echo "$mailSubject";
        echo "";
        echo "$mailBody";
        sleep 1;
        echo "hello";
        echo ".";
        sleep 1;
        echo "QUIT"; ) | telnet
fi
}
mailSsl(){
if [ "$authProtocol" = "s" ] || [ "$authProtocol" = "S" ] || [ "$authProtocol" = "SSL" ] || [ "$authProtocol" = "ssl" ];
then
       (
        sleep 2;
        echo "EHLO $smtpServer";
if [ "$authMode" = "y" ]; then
        echo "AUTH LOGIN";
	sleep 1;
        echo "$encryptUser";
        sleep 1;
        echo "$encryptPass";
fi
	sleep 1;
        echo -e "MAIL FROM:<$senderMail>";
        sleep 1;
        echo -e "RCPT TO:<$receiverMail>";
        sleep 1;
        echo "DATA";
        echo "From:<$senderMail>";
        echo "To:<$receiverMail>";
        sleep 1;
	echo "$mailSubject";
	echo "";
	echo "$mailBody";
	sleep 1;
	echo ".";
	sleep 1;
	echo "QUIT"; ) | openssl s_client -connect smtp.gmail.com:465 -crlf -ign_eof -quiet
fi
}
mailSendGrid(){
if [ "$authProtocol" = "n" ] ;
then
	export SENDGRID_API_KEY='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'
	curl --request POST \
	--url https://api.sendgrid.com/v3/mail/send \
	--header "Authorization: Bearer $SENDGRID_API_KEY" \
	--header 'Content-Type: application/json' \
	--data '{"personalizations": [{"to": [{"email": "$receiverMail"}]}],"from": {"email": "senderMail"},"subject": "$mailSubject","content": [{"type": "text/plain", "value": "and easy to do anywhere, even with cURL"}]}'
fi
}
case "$stats" in
	"conf" )
		if [ "$authProtocol" = "t" ] || [ "$authProtocol" = "T" ] || [ "$authProtocol" = "TLS" ] || [ "$authProtocol" = "tls" ] && [ "$#" -eq 0 ]; then
			mailTls
		else
			echo "Not fully configured for TLS:"
		fi
		if [ "$authProtocol" = "s" ] || [ "$authProtocol" = "S" ] || [ "$authProtocol" = "SSL" ] || [ "$authProtocol" = "ssl" ] && [ "$#" -eq 0 ]; then
			mailSsl
		else
			echo "Not fully configured for SSL:"
		fi
		if [ "$authProtocol" = "n" ] || [ "$authProtocol" = "N" ] && [ "$#" -eq 0 ]; then
			mailSendGrid
		else
			echo "Not fully configured sendGrid:"
		fi
		;;
esac
if [ -n "$stats" ] && [ -z "$1" ]; then
	echo -e "Setting is not Configured. Run: \"$0 conf\""
	echo -e "Or use: \"$0 reconf\" to reset configuration"
	exit;
fi
	if [ "$1" = "reconf" ] && [ "$stats" = "notConf" ];
	then
        	sed $n -e "0,/##VRREND/ c\#!/bin/bash\n##TMPVRR\n#smtpServer\n#smtpPort\n#senderMail\n#receiverMail\n#sleepBetween\n#authMode\n#authProtocol\n#authUser\n#authPass\nstats=notConf\n##VRREND" $0
	exit;
	fi
